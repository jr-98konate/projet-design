import React from 'react';
import {Link} from "react-router-dom";
import image7 from "../img/bakeli.png";
import "./Navbar.css";

import connexion from "../img/connexion.png";

function Navbar() {
  return (
	<div>
		  <nav className="navbar navbar-expand-lg navbar-fixed-top border-0 shadow bg-white ">
			  <div className="container-fluid">
				  <Link className="navbar-brand" to="/"><img src={image7} alt="logo" className='logo mb-3 ' /> </Link>
				  <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
					  <span className="navbar-toggler-icon"></span>
				  </button>
				  <div className="collapse navbar-collapse" id="navbarNav">
					  <ul className="navbar-nav ms-auto">
						  
					  <div className="dropdown py-3 ps-3 pe-5">
							  <button className=" border-0 bg-white dropdown-toggle pt-3" type="button" data-bs-toggle="dropdown" aria-expanded="false">
							  <span className="pe-3"><img src={connexion} alt='connexion' className="mb-1 black" style={{
								width:"300px;",
								height: "auto"
							  }} /></span>
							 	 Connexion 
							  </button>
							  
							  <ul className="dropdown-menu">
								  <li>
									<Link className="dropdown-item" to="/signin">
										Connexion 
										
									  </Link>
								  </li>
								  <li><Link className="dropdown-item" to="/signup">Inscription</Link></li>
								  
							  </ul>
						  </div>
						  
					  </ul>
				  </div>
				  
			  </div>
		  </nav>
	</div>
  )
}

export default Navbar