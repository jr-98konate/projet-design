import React from 'react'

import "./HeaderNavbar.css";
import groupe from "../img/groupe.png"
import {FcGoogle} from "react-icons/fc"
import objective from "../img/change.png";

import img51 from "../img/Rectangle51.png"
import img50 from "../img/Rectangle50.png"
import rectangle55 from "../img/Rectangle 55.png";
import rectangle56 from "../img/Rectangle 56.png";
import rectangle57 from "../img/Rectangle 57.png";
import Rectangle60 from "../img/Rectangle60.png";
import Rectangle61 from "../img/Rectangle 63.png";
import Rectangle62 from "../img/Rectangle 66.png";


import image10 from "../img/image10.png";
import user from "../img/user.png";

import carousel1 from "../img/carousel1.png"
import carousel2 from "../img/carousel2.png"
import carousel3 from "../img/carousel3.png";
import {BsSearch} from "react-icons/bs"
import iscrit from "../img/inscrit.png";
import cour from "../img/cour.png";


 
function HeaderNavbar() {
  return (
	  <div>
		  {/*---------------------nav------------------- */}
		  <div className='container  mt-5'>
			  <div className="form-group row justify-content-center">
				  <div className=" col-lg-7 col-md-7 col-sm-4 col-xs-4 ">
					  <div className="input-group">
						  <input type="text" id="form1" className="bg-light form-control" aria-label='Disabled input example' />
						  <button type="button" className="btn btn-lg btn-success">
							  <BsSearch />
						  </button>
					  </div>
				  </div>
			  </div>
			  <h3 className='mt-5'>
				  Evénements carrière à ne pas manquer
			  </h3>
		  </div>



		  <div className='container carousel my-3 '>

			  {/*---------------------carousel------------------- */}
			  <div id="carouselExampleSlidesOnly" className="carousel slide" data-bs-ride="carousel">
				  <div className="carousel-inner">
					  <div className="carousel-item active">
						  <div className=" card center border-0 shadow " >
							  <div className="row ">
								  <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12  ">
									  <div className="card card border-0">
										  <h3 className="card-title l my-4 ms-5">DU JEUDI  22 SEPTEMBRE 2022 Ä 10:00 AU SAMEDI 22 OCTOBRE 2022 <br />
											  Ä 18:00</h3>
										  <h3 className="card-text ms-2 my-2 google ms-5">
											  Google warsaw - Women in tech mentoring program
										  </h3>
										  <p className="card-text my-3 ms-5">
											  <small className="text-muted">En ligne</small>
										  </p>
										  <p className=" my-3 ms-5"><span className="">
											  <FcGoogle className='' style={{ fontSize: "30px" }} /> </span> <span className="ms-3">Google</span>
										  </p>
										  <p className=" my-3 ms-5"><span className=" "><img src={iscrit} alt="ins"
											  style={{ width: "50px;", height: "auto" }} /> </span> <span className="ms-3">Inscription closes</span></p>
									  </div>
								  </div>
								  <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									  <div className='card'>
										  <img
											  src={groupe}
											  alt="Trendy Pants and Shoes"
											  className="img-fluid rounded-start"
											  style={{ width: "100%", height: "390px" }}
										  />
									  </div>
								  </div>
							  </div>
						  </div>
					  </div>
					  <div className="carousel-item ">
						  <div className=" card center border-0 shadow " >
							  <div className="row ">
								  <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
									  <div className="card card border-0">
										  <h3 className="card-title l my-4 ms-5">DU JEUDI  22 SEPTEMBRE 2022 Ä 10:00 AU SAMEDI 22 OCTOBRE 2022 <br />
											  Ä 18:00</h3>
										  <h3 className="card-text ms-2 my-2 google ms-5">
											  Google warsaw - Women in tech mentoring program
										  </h3>
										  <p className="card-text my-3 ms-5">
											  <small className="text-muted">En ligne</small>
										  </p>
										  <p className=" my-3 ms-5"><span className="">
											  <FcGoogle className='' style={{ fontSize: "30px" }} /> </span> <span className="ms-3">Google</span>
										  </p>
										  <p className=" my-3 ms-5"><span className=" "><img src={iscrit} alt="ins"
											  style={{ width: "50px;", height: "auto" }} /> </span> <span className="ms-3">Inscription closes</span></p>
									  </div>
								  </div>
								  <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									  <div className='card'>
										  <img
											  src={carousel1}
											  alt="Trendy Pants and Shoes"
											  className="img-fluid rounded-start w-100"
											  style={{ width: "100%", height: "100%" }}
										  />
									  </div>
								  </div>
							  </div>
						  </div>
					  </div>
					  <div className="carousel-item ">
						  <div className=" card center border-0 shadow " >
							  <div className="row ">
								  <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
									  <div className="card card border-0">
										  <h3 className="card-title l my-4 ms-5">DU JEUDI  22 SEPTEMBRE 2022 Ä 10:00 AU SAMEDI 22 OCTOBRE 2022 <br />
											  Ä 18:00</h3>
										  <h3 className="card-text ms-2 my-2 google ms-5">
											  Google warsaw - Women in tech mentoring program
										  </h3>
										  <p className="card-text my-3 ms-5">
											  <small className="text-muted">En ligne</small>
										  </p>
										  <p className=" my-3 ms-5"><span className="">
											  <FcGoogle className='' style={{ fontSize: "30px" }} /> </span> <span className="ms-3">Google</span>
										  </p>
										  <p className=" my-3 ms-5"><span className=" "><img src={iscrit} alt="ins"
											  style={{ width: "50px;", height: "auto" }} /> </span> <span className="ms-3">Inscription closes</span></p>
									  </div>
								  </div>
								  <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									  <div className='card'>
										  <img
											  src={carousel2}
											  alt="Trendy Pants and Shoes"
											  className="img-fluid rounded-start w-100"
											  style={{ width: "100%", height: "100%" }}
										  />
									  </div>
								  </div>
							  </div>
						  </div>
					  </div>
					  <div className="carousel-item ">
						  <div className=" card bg-white border-0 shadow " >
							  <div className="row ">
								  <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
									  <div className="card card border-0">
										  <h3 className="card-title l my-4 ms-5">DU JEUDI  22 SEPTEMBRE 2022 Ä 10:00 AU SAMEDI 22 OCTOBRE 2022 <br />
											  Ä 18:00</h3>
										  <h3 className="card-text ms-2 my-2 google ms-5">
											  Google warsaw - Women in tech mentoring program
										  </h3>
										  <p className="card-text my-3 ms-5">
											  <small className="text-muted">En ligne</small>
										  </p>
										  <p className=" my-3 ms-5"><span className="">
											  <FcGoogle className='' style={{ fontSize: "30px" }} /> </span> <span className="ms-3">Google</span>
										  </p>
										  <p className=" my-3 ms-5"><span className=" "><img src={iscrit} alt="ins"
											  style={{ width: "50px;", height: "auto" }} /> </span> <span className="ms-3">Inscription closes</span></p>
									  </div>
								  </div>
								  <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									  <div className='card'>
										  <img
											  src={carousel3}
											  alt="Trendy Pants and Shoes"
											  className="img-fluid rounded-start w-100"
											  style={{ width: "100%", height: "100%" }}
										  />
									  </div>
								  </div>
							  </div>
						  </div>
					  </div>
				  </div>
			  </div>

		  </div>
		  <div className="container">
			  <h2 className='mt-5'>
				  Accompagnement carrière
			  </h2>

		  </div>

		  {/*---------------------card1------------------- */}
		  <div className='container   my-5 carte1'>
			  <div className='row mx-auto my-5 parti'>
				  <div className='col-lg-4 col-md-4 col-sm-12 col-xs-12 over'>
					  <div className='card my-2 border-0 shadow '>
						  <img src={objective} alt="object" className="img-fluid carte " />
						  <div className='card-body'>
							  <h6 className="title-card">Le Jeudi 13 OCTOBRE 2022 DE 10:00 A 10:45 </h6>
							  <p className='text'>Dealing witch sollers</p>
							  <p className='text'>En ligne</p>
							  <p className='text'><span><img src={image10} alt="soller" className="soler" /></span><span className='ms-2'>
								  Soller consulting</span>
							  </p>
							  <hr className="w-100" />
							  <p className='text'><span><img src={user} alt="soller" className="user" /></span><span className='ms-2'>
								  56 étudiant sont intressée</span>
							  </p>
						  </div>
					  </div>
				  </div>
				  <div className='col-lg-4 col-md-4 col-sm-12 col-xs-12 over'>
					  <div className='card my-2 border-0 shadow '>
						  <img src={img50} alt="object" className="img-fluid carte" />
						  <div className='card-body'>
							  <h6 className="title-card">Le Jeudi 13 OCTOBRE 2022 DE 10:00 A 10:45 </h6>
							  <p className='text'>Dealing witch sollers</p>
							  <p className='text'>En ligne</p>
							  <p className='text'><span><img src={image10} alt="soller" className="soler" /></span><span className='ms-2'>
								  Soller consulting</span>
							  </p>
							  <hr />
							  <p className='text'><span><img src={cour} alt="soller" className="user" /></span><span className='ms-2'>
								  En cours Termine dans 8 jours</span>
							  </p>
						  </div>
					  </div>
				  </div>
				  <div className='col-lg-4 col-md-4 col-sm-12 col-xs-12 over'>
					  <div className='card my-2 border-0 shadow '>
						  <img src={img51} alt="object" className="img-fluid carte" />
						  <div className='card-body'>
							  <h6 className="title-card">Le Jeudi 13 OCTOBRE 2022 DE 10:00 A 10:45 </h6>
							  <p className='text'>Dealing witch sollers</p>
							  <p className='text'>En ligne</p>
							  <p className='text'><span><img src={image10} alt="soller" className="soler" /></span><span className='ms-2'>
								  Soller consulting</span>
							  </p>
							  <hr />
							  <p className='text'><span><img src={cour} alt="soller" className="user" /></span><span className='ms-2'>
								  En cours Termine dans 7 jours</span>
							  </p>
						  </div>
					  </div>
				  </div>
				  <h4 className='my-4 text-success'>Voir les  événements</h4>
				  <h2 className='my-5'>Rencontres professionnelles et recrutement</h2>
			  </div>
		  </div>
		  {/*---------------------card2------------------- */}
		  <div className='container parti my-5'>
			  <div className='row my-5'>
				  <div className='col-lg-4 col-md-4 col-sm-12 col-xs-12 over '>
					  <div className='card my-2 border-0 shadow '>
						  <img src={rectangle55} alt="object" className="img-fluid " />
						  <div className='card-body'>
							  <h6 className="title-card text-center">Le Jeudi 13 OCTOBRE 2022 DE 10:00 A 10:45 </h6>
							  <p className='text-justify'>Dealing witch sollers</p>
							  <p className='text-justify'>En ligne</p>
							  <p className='text'><span><img src={image10} alt="soller" className="soler" /></span><span className='ms-2'>
								  Soller consulting</span>
							  </p>
							  <hr />
							  <p className='text'><span><img src={user} alt="soller" className="user" /></span><span className='ms-2'>
								  56 étudiant sont intressée</span>
							  </p>
						  </div>
					  </div>
				  </div>
				  <div className='col-lg-4 col-md-4 col-sm-12 col-xs-12 over '>
					  <div className='card my-2 border-0 shadow '>
						  <img src={rectangle56} alt="object" className="img-fluid " />
						  <div className='card-body'>
							  <h6 className="title-card text-center">Le Jeudi 13 OCTOBRE 2022 DE 10:00 A 10:45 </h6>
							  <p className='text-justify'>Dealing witch sollers</p>
							  <p className='text-justify'>En ligne</p>
							  <p className='text'><span><img src={image10} alt="soller" className="soler" /></span><span className='ms-2'>
								  Soller consulting</span>
							  </p>
							  <hr />
							  <p className='text'><span><img src={user} alt="soller" className="user" /></span><span className='ms-2'>
								  56 étudiant sont intressée</span>
							  </p>
						  </div>
					  </div>
				  </div>
				  <div className='col-lg-4 col-md-4 col-sm-12 col-xs-12 over'>
					  <div className='card my-2 border-0 shadow '>
						  <img src={rectangle57} alt="object" className="img-fluid" />
						  <div className='card-body'>
							  <h6 className="title-card text-center">Le Jeudi 13 OCTOBRE 2022 DE 10:00 A 10:45 </h6>
							  <p className='text'>Dealing witch sollers</p>
							  <p className='text'>En ligne</p>
							  <p className='text'><span><img src={image10} alt="soller" className="soler" /></span><span className='ms-2'>
								  Soller consulting</span>
							  </p>
							  <hr />
							  <p className='text'><span><img src={cour} alt="soller" className="user" /></span><span className='ms-2'>
								  En cours Termine dans 7 jours</span>
							  </p>
						  </div>
					  </div>
				  </div>
			  </div>
		  </div>
		  {/*---------------------card3------------------- */}
		  <div className='container my-5'>
			  <div className='row parti my-5'>
				  <div className='col-lg-4 col-md-4 col-sm-12 col-xs-12 over'>
					  <div className='card my-2 border-0 shadow '>
						  <img src={Rectangle60} alt="object" className="img-fluid " style={{ height: "50px;" }} />
						  <div className='card-body'>
							  <h6 className="title-card">Le Jeudi 13 OCTOBRE 2022 DE 10:00 A 10:45 </h6>
							  <p className='text'>Dealing witch sollers</p>
							  <p className='text'>En ligne</p>
							  <p className='text'><span><img src={image10} alt="soller" className="soler" /></span><span className='ms-2'>
								  Soller consulting</span>
							  </p>
							  <hr />
							  <p className='text'><span><img src={user} alt="soller" className="user" /></span><span className='ms-2'>
								  56 étudiant sont intressée</span>
							  </p>
						  </div>
					  </div>
				  </div>
				  <div className='col-lg-4 col-md-4 col-sm-12 col-xs-12 over'>
					  <div className='card my-2 border-0 shadow '>
						  <img src={Rectangle61} alt="object" className="img-fluid " />
						  <div className='card-body'>
							  <h6 className="title-card">Le Jeudi 13 OCTOBRE 2022 DE 10:00 A 10:45 </h6>
							  <p className='text'>Dealing witch sollers</p>
							  <p className='text'>En ligne</p>
							  <p className='text'><span><img src={image10} alt="soller" className="soler" /></span><span className='ms-2'>
								  Soller consulting</span>
							  </p>
							  <hr />
							  <p className='text'><span><img src={user} alt="soller" className="user" /></span><span className='ms-2'>
								  56 étudiant sont intressée</span>
							  </p>
						  </div>
					  </div>
				  </div>
				  <div className='col-lg-4 col-md-4 col-sm-12 col-xs-12 over'>
					  <div className='card my-2 border-0 shadow '>
						  <img src={Rectangle62} alt="object" className="img-fluid" />
						  <div className='card-body'>
							  <h6 className="title-card">Le Jeudi 13 OCTOBRE 2022 DE 10:00 A 10:45 </h6>
							  <p className='text'>Dealing witch sollers</p>
							  <p className='text'>En ligne</p>
							  <p className='text'><span><img src={image10} alt="soller" className="soler" /></span><span className='ms-2'>
								  Soller consulting</span>
							  </p>
							  <hr />
							  <p className='text'><span><img src={user} alt="soller" className="user" /></span><span className='ms-2'>
								  56 étudiant sont intressée</span>
							  </p>
						  </div>
					  </div>
				  </div>
			  </div>
			  <h4 className="text-success">Voir les 7 évenements</h4>
		  </div>
	  </div>
	
  )
}

export default HeaderNavbar