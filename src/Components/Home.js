import React from 'react'
import Footer from './Footer'
import HeaderNavbar from './HeaderNavbar'
import Navbar from './Navbar'

function Home() {
  return (
	<div className='home'>
		<Navbar/>
        <HeaderNavbar className="my-5"/>
		<Footer/>
	</div>
  )
}

export default Home 