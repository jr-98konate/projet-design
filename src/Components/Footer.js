import React from 'react'
import {Link} from "react-router-dom";
import vector6 from "../img/Vector6.png";
import vector7 from "../img/Vector7.png";
import vector8 from "../img/Vector8.png";
import image7 from "../img/bakeli.png";
function Footer() {
  return (
	<>
		<footer className="container-fluid my-5 mt-5  bg-black ">
				<div className='container-fluid my-4 conte-un'>
					<div className='row '>
						<div className=' col-md-9 col-sm-12 col-xs-12 '>
							<img src={image7} alt="logo" className='logo h-50'/>
						</div>
						<div className='col-md-3 col-sm-12 col-xs-12-center text '>
							<div className='nav text-justify-end '>
								
								<li className='nav-item my-4'>
								<Link className='nav-link vector1'>
									<img src={vector6} alt="vector1" className=''/>

								</Link>
								</li>
								<li className='nav-item my-4'>
								<Link className='nav-link vector1'>
									<img src={vector7} alt="vector1" className=''/>

								</Link>

								</li>
								<li className='nav-item my-4'>
								<Link className=' nav-link vector1'>
									<img src={vector8} alt="vector1" className=''/>

								</Link>
								</li>
							</div>							
						</div>
					</div>
				</div>		 
			  <div className="container-fluid my-4 conte-list">
				<div className='row text-center  footer-list'>
					<div className='col-lg-3 col-md-3 col-sm-12 col-xs-12'>
						<ul className='text-justify'>
							<li className=''>
							S'inscrire <br />
							Chercher une offre<br />
							Découvrir les entreprises<br />
							Evénements recrutement<br />
							Conseils recrutement<br />
							</li>
						</ul>
					</div>
					<div className='col-lg-3 col-md-3 col-sm-12 col-xs-12'>
					<ul className='text-justify'>
							<li className=''>
							Écoles & Universités <br/>

							Notre offre Career Center<br/>
							Nos établissements partenaires<br/>

							</li>
						</ul>
					</div>
					<div className='col-lg-3 col-md-3 col-sm-12 col-xs-12'>
					<ul className='text-justify'>
							<li className=''>
							Entreprises <br/>

							Notre offre Entreprise


							</li>
						</ul>
					</div>
					<div className='col-lg-3 col-md-3 col-sm-12 col-xs-12'>
						<ul className='text-justify'>
							<li>
							JobTeaser <br/>

							Nous rejoindre<br/>
							À propos<br/>
							Rencontrer notre équipe<br/>
							</li>
						</ul>
					</div>
				</div>
			  </div>
			  <div className="container-fluid rang">
					<div className='row text-white text-center'>
						<div className='col-lg-1 col-md-1 col-sm-12 col-xs-12'>Statuts </div>
						<div className='col-lg-2 col-md-2 col-sm-12 col-xs-12'>Mentions légales </div>
						<div className='col-lg-1 col-md-1 col-sm-12 col-xs-12'>Cookies</div>
						<div className='col-lg-3 col-md-3 col-sm-12 col-xs-12'>Politique de confidentialité</div>
						<div className='col-lg-2 col-md-2'>Securité</div>
						<div className='col-lg-3 col-md-3'>Copyright © JobTeaser 2008-2022 </div>
					</div>
			  </div> 
			</footer>

	</>
  )
}

export default Footer