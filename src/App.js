import React from 'react';
import "./Components/HeaderNavbar.css";
import {Routes, Route} from "react-router-dom"
import Home from './Components/Home';
import SignIn from './Access/SignIn';
import SignUp from './Access/SignUp';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path='/' element={<Home/>} />
        <Route path='/signin' element={<SignIn/>}/>
        <Route path="/signup" element={<SignUp/>}/>
      </Routes>
        
    </div>
  );
}

export default App;
