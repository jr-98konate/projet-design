import React from 'react'
import "./SignUp.css"
import Navbar from "../Components/Navbar.js"
import Footer from "../Components/Footer.js"
import {Link} from "react-router-dom"
function SignUp() {
  return (
	<>
    <Navbar/>
      <section className="h-100 gradient-form" >
        <div className="container py-5 h-100">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col-xl-12">
              <div className="row g-0">
                <div className="col-lg-6">
                  <div className="text-justify tete mt-1 mb-5 pb-1">
                    <h1 className="">Inscription</h1>
                    <p>L'inscription est rapide et gratuite. Rejoignez la communauté !</p>
                  </div>
                  <form>
                    <div className="form-outline mb-4">
                      <label className="form-label" for="form2Example11"><h5>Nom Complet</h5></label>
                      <input type="text" id="name" className="form-control form-control-lg bg-light "
                      />
                    </div>
                    <div className="form-outline mb-4">
                      <label className="form-label" for="form2Example11"><h5>Email</h5></label>
                      <input type="email" id="email" className="form-control form-control-lg bg-light"
                      />
                    </div>
                    <div className="form-outline mb-4">
                      <label className="form-label" for="form2Example11"><h5>Mot passe</h5></label>
                      <input type="password" id="psw" className="form-control form-control-lg bg-light" 
                      />
                    </div>
                    <div className="form-outline mb-4">
                      <label className="form-label" for="form2Example11"><h5>Confirmer Mot passe</h5></label>
                      <input type="password" id="psw" className="form-control form-control-lg bg-light"
                      />
                    </div>
                    <div className="form-outline mb-4 text-center">
                      <button type="submit" className="btn  w-100 button1">M'inscrire</button>
                    </div>
                    <div className="text-center">
                    <p>Vous avez deja Un compte? <Link to="/signin" className="text-success link-ver">Connectez Vous</Link></p>
                    </div>
                    
                    
                  </form>


                </div>
                <div className="col-lg-6 d-flex align-items-center left ">
                  <div className="text-black px-3 py-4 p-md-2 mx-md-4 mt-5 global-left ">
                    <p className=" "> <h5 className='my-2'>Vous êtes déjà inscrit.e ?Connectez-vous</h5> <br/>
                    <span className='mt-5 top'>
                    En souscrivant à nos services, vous recevrez des emails de notre part pour vous accompagner sur l’orientation et le recrutement. Vous pouvez vous désinscrire à tout moment de ces communications via la lien de désinscription dans l’email reçu.
                      Les informations recueillies font l’objet d’un traitement informatique destinées à JobTeaser et ses partenaires (Ecoles et Université d’une part, Recruteurs de l’autre) afin de nous permettre de répondre à vos demandes, de gérer votre compte utilisateur et de vous proposer les meilleurs services de recherche de stage ou d’emploi possible.
                      Conformément à la réglementation applicable en matière de protection des données personnelles, vous bénéficiez d'un droit d'accès et de rectification aux informations vous concernant que vous pouvez exercer conformément aux modalités décrites dans notre politique de confidentialité, dont nous vous invitons à prendre connaissance. Vous pouvez également, pour des motifs légitimes, vous opposer au traitement des données vous concernant.

                    </span>
                      
                    </p>

                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </section>
    <Footer/>
  </>
  )
}

export default SignUp